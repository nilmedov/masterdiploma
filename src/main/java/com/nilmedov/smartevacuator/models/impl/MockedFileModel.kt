package com.nilmedov.smartevacuator.models.impl

import com.nilmedov.smartevacuator.models.FileModel
import java.io.File

class MockedFileModel : FileModel {

    override fun getFiles(): List<File> {
        return mutableListOf(
                File("mocked_file1.json"),
                File("mocked_file2.json"),
                File("mocked_file3.xml")
        )
    }
}