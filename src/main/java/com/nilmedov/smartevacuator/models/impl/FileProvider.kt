package com.nilmedov.smartevacuator.models.impl

import com.nilmedov.smartevacuator.models.FileModel
import java.io.File

class FileProvider : FileModel {

    override fun getFiles(): List<File> {
        //TODO IS_DEBUG = false doesn't work with jar
//        val path = if (SystemUtils.IS_DEBUG) "./target" else "."
        val path = "./target"
        val folder = File(path)
        return folder.listFiles().filter { file -> !file.isDirectory }
    }


}