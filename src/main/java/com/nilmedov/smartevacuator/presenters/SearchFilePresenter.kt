package com.nilmedov.smartevacuator.presenters

import com.nilmedov.smartevacuator.views.SearchFileView

interface SearchFilePresenter : Presenter<SearchFileView> {

    fun onFileSelected(position: Int)

    fun onRefreshFiles()
}