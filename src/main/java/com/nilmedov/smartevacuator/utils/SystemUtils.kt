package com.nilmedov.smartevacuator.utils

import com.google.gson.Gson
import java.awt.GraphicsEnvironment


class SystemUtils {

    companion object {

        var IS_DEBUG  = true

        var MOCK_RESOURCES = false

        val DISPLAY_PADDING = 100

        val DISPLAY_MODE = GraphicsEnvironment.getLocalGraphicsEnvironment().defaultScreenDevice.displayMode!!

        val JSON_MAPPER = Gson()
    }
}