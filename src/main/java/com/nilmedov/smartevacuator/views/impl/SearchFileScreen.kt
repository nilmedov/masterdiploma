package com.nilmedov.smartevacuator.views.impl

import com.nilmedov.smartevacuator.presenters.SearchFilePresenter
import com.nilmedov.smartevacuator.utils.Log
import com.nilmedov.smartevacuator.utils.Strings
import com.nilmedov.smartevacuator.utils.SystemUtils
import com.nilmedov.smartevacuator.views.SearchFileView
import javafx.collections.FXCollections
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.ListView
import javafx.scene.layout.VBox
import javafx.scene.paint.Color
import javafx.stage.Stage
import java.io.File

class SearchFileScreen(primaryStage: Stage, presenter: SearchFilePresenter) : SearchFileView {

    companion object {
        private val TAG = SearchFileScreen::class.java.simpleName
        private val CLICK_COUNT = 2
    }

    private var scene: Scene
    private var presenter: SearchFilePresenter

    private val filesListView: ListView<String> = ListView()
    private val btnRefresh: Button = Button(Strings.REFRESH)

    init {
        val pane = VBox()
        pane.children.addAll(filesListView, btnRefresh)

        scene = Scene(pane,
                SystemUtils.DISPLAY_MODE.width.toDouble() - SystemUtils.DISPLAY_PADDING,
                SystemUtils.DISPLAY_MODE.height.toDouble() -  SystemUtils.DISPLAY_PADDING)
        scene.fill = Color.WHITE
        primaryStage.scene = scene
        primaryStage.title = Strings.SEARCH_FILE_VIEW_TITLE


        this.presenter = presenter
        this.presenter.onAttach(this)

        setupListeners()
    }

    override fun showFiles(files: List<File>) {
        Log.d(TAG, "showFiles() $files")
        filesListView.items = FXCollections.observableList(files.map { file -> file.name })
    }

    override fun release() {
        System.out.println("release()")
        presenter.onDetach()
    }

    private fun setupListeners() {
        btnRefresh.onMouseClicked = EventHandler { presenter.onRefreshFiles() }
        filesListView.onMouseClicked = EventHandler {
            if (it.clickCount == CLICK_COUNT) {
                presenter.onFileSelected(filesListView.selectionModel.selectedIndex)
            }
        }
    }
}