package com.nilmedov.smartevacuator.models

import java.io.File

interface FileModel {

    fun getFiles() : List<File>
}