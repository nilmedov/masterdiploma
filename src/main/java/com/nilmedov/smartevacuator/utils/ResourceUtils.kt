package com.nilmedov.smartevacuator.utils

class ResourceUtils {

    companion object {
        val CSS_STYLE_NODE = "node"
        val CSS_STYLE_ROOM = "room"
        val CSS_STYLE_EXIT = "exit"
        val CSS_STYLE_PATH = "path"
        private val CSS_GRAPH = "graph.css"

        fun getResourcePath(fileName: String) : String? {
            return Thread.currentThread().contextClassLoader.getResource(fileName).file
        }

        fun getCSSPath(): String? {
            return getResourcePath(CSS_GRAPH)?.let {
                "url('$it')"
            }
        }
    }
}