package com.nilmedov.smartevacuator.main

import com.nilmedov.smartevacuator.models.impl.FileProvider
import com.nilmedov.smartevacuator.models.impl.MockedFileModel
import com.nilmedov.smartevacuator.presenters.impl.SearchFilePresenterImpl
import com.nilmedov.smartevacuator.utils.SystemUtils
import com.nilmedov.smartevacuator.views.View
import com.nilmedov.smartevacuator.views.impl.SearchFileScreen
import javafx.application.Application
import javafx.stage.Stage

class SearchFileApplication : Application() {

    private var activeView : View? = null

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            Application.launch(SearchFileApplication::class.java)
        }
    }

    override fun start(primaryStage: Stage) {
        val fileModel = if (!SystemUtils.MOCK_RESOURCES) FileProvider() else MockedFileModel()
        activeView = SearchFileScreen(primaryStage, SearchFilePresenterImpl(
                fileModel,
                {
                    GraphApplication.start(it)
                    primaryStage.toBack()
                }
        ))
        primaryStage.show()
    }

    override fun stop() {
        super.stop()
        activeView?.release()
    }
}
