package com.nilmedov.smartevacuator.utils

class Log {

    companion object {
        fun d(tag: String, message: String?) {
            System.out.println("$tag: $message")
        }
    }
}