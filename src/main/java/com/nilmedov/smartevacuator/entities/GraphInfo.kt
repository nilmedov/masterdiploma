package com.nilmedov.smartevacuator.entities

data class GraphInfo(
        val exits: List<Exit>,
        val rooms: List<Room>,
        val cells: List<Cell>)