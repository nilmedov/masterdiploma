package com.nilmedov.smartevacuator.views

import java.io.File

interface SearchFileView : View {

    fun showFiles(files: List<File>)
}