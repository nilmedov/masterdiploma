package com.nilmedov.smartevacuator.entities

abstract class Node {

    abstract val id: String

    abstract val neighbors: List<String>
}