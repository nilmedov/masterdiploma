package com.nilmedov.smartevacuator.views

import com.nilmedov.smartevacuator.entities.GraphInfo

interface GraphFloorView : View {

    fun setFloorGraph(graphInfo: GraphInfo)
}