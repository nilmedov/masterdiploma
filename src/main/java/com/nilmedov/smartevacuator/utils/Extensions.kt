package com.nilmedov.smartevacuator.utils

import org.graphstream.graph.Node

internal object ExtensionConsts {
    val STORED_NODE = "stored.node"
}

fun Node.setName(name: String) : Node {
    addAttribute("ui.label", name)
    return this
}

fun Node.setStyle(styleName: String) : Node {
    addAttribute("ui.class", styleName)
    return this
}

fun Node.setStoredNode(node: com.nilmedov.smartevacuator.entities.Node) : Node {
    setAttribute(ExtensionConsts.STORED_NODE, node)
    return this
}

fun Node.getStoredNode() : com.nilmedov.smartevacuator.entities.Node {
    return getAttribute(ExtensionConsts.STORED_NODE)
}