package com.nilmedov.smartevacuator.entities

data class Exit(
        override val id: String = "E",
        override val neighbors: List<String> = ArrayList()) : Node()