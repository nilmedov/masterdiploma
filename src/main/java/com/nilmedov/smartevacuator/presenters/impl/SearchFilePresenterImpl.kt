package com.nilmedov.smartevacuator.presenters.impl

import com.nilmedov.smartevacuator.models.FileModel
import com.nilmedov.smartevacuator.presenters.SearchFilePresenter
import com.nilmedov.smartevacuator.utils.Log
import com.nilmedov.smartevacuator.views.SearchFileView
import java.io.File

class SearchFilePresenterImpl(fileModel: FileModel, appDelegate: ((String) -> Unit)?) : SearchFilePresenter {

    companion object {
        private val TAG = SearchFilePresenterImpl::class.java.simpleName
    }

    private var view: SearchFileView? = null
    private var fileModel: FileModel? = fileModel
    private var appDelegate: ((String) -> Unit)? = appDelegate

    private var files : List<File>? = null


    override fun onAttach(view: SearchFileView) {
        this.view = view
        onRefreshFiles()
    }

    override fun onDetach() {
        files = null
        view = null
        fileModel = null
        appDelegate = null

    }

    override fun onFileSelected(position: Int) {
        Log.d(TAG, "onFileSelected $position")
        appDelegate?.invoke(files!![position].path)
    }

    override fun onRefreshFiles() {

        files = fileModel?.getFiles()?.let {
            val filtered = it.filter { file -> file.extension == "json" }
            view?.showFiles(filtered)
            filtered
        }
        Log.d(TAG, "onRefreshFiles() $files")
    }
}