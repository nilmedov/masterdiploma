package com.nilmedov.smartevacuator.main

import com.nilmedov.smartevacuator.entities.GraphInfo
import com.nilmedov.smartevacuator.utils.SystemUtils
import com.nilmedov.smartevacuator.views.impl.GraphFloorScreen
import java.io.File

class GraphApplication {
    companion object {

        fun start(filePath: String) {
            main(arrayOf(filePath))
        }

        @JvmStatic fun main(args: Array<String>) {

            //TODO: Validate file
            val filePath = if (args.isNotEmpty()) args[0] else ""
            println("GraphApplication started with $filePath")

            System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer")

            init(File(filePath))
        }

        @JvmStatic private fun init(file: File) {

            val view = GraphFloorScreen()
            val graphInfo = SystemUtils.JSON_MAPPER.fromJson<GraphInfo>(file.reader(), GraphInfo::class.java)

            view.setFloorGraph(graphInfo)
        }
    }
}