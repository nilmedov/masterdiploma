package com.nilmedov.smartevacuator.entities

data class Cell(
        override val id: String = "C",
        override val neighbors: List<String> = ArrayList()) : Node()