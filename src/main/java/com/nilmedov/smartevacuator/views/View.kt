package com.nilmedov.smartevacuator.views

interface View {

    fun release()
}