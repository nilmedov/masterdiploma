package com.nilmedov.smartevacuator.presenters

import com.nilmedov.smartevacuator.views.View

interface Presenter<in T : View> {

    fun onAttach(view: T)

    fun onDetach()
}