package com.nilmedov.smartevacuator.utils

class Strings {

    companion object {
        val SEARCH_FILE_VIEW_TITLE = "Оберіть файл плану будівлі (.json)"
        val REFRESH = "Оновити"

        val EXAMPLE_JSON = "{\"exits\":[{\"id\":\"E1\",\"neighbors\":[\"C1\"]}],\"rooms\":[{\"id\":\"R1\",\"neighbors\":[\"C2\"],\"peoples\":5},{\"id\":\"R2\",\"neighbors\":[\"C3\"],\"peoples\":2}],\"cells\":[{\"id\":\"C1\",\"neighbors\":[\"C2\",\"C3\"]},{\"id\":\"C2\",\"neighbors\":[]},{\"id\":\"C3\",\"neighbors\":[]}]}"
    }
}