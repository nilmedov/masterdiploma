package com.nilmedov.smartevacuator.utils

import org.graphstream.ui.view.ViewerPipe
import java.util.concurrent.Executors


class EventHandler (pipe: ViewerPipe) : Runnable {

    companion object {
        private var instance: EventHandler? = null

        fun submit(pipe: ViewerPipe) {
            var localInstance = instance
            if (localInstance == null) {
                synchronized(EventHandler) {
                    localInstance = instance
                    if (localInstance == null) {
                        instance = EventHandler(pipe)
                    } else {
                        localInstance?.run {
                            invalidate()
                            this.pipe = pipe
                        }
                    }
                }
            } else {
                localInstance?.run {
                    invalidate()
                    this.pipe = pipe
                }
            }

            instance?.submit()
        }

        fun invalidate() {
            instance?.run {
                this.loop = false
                this.pipe = null
            }
        }
    }

    private val executorService = Executors.newSingleThreadExecutor()
    private var pipe: ViewerPipe? = pipe
    private var loop = false

    override fun run() {
        loop = true
        while (loop) {
            pipe?.blockingPump()
        }
    }

    fun submit() {
        executorService.submit(this)
    }
}