package com.nilmedov.smartevacuator.views.impl

import com.nilmedov.smartevacuator.entities.Cell
import com.nilmedov.smartevacuator.entities.Exit
import com.nilmedov.smartevacuator.entities.GraphInfo
import com.nilmedov.smartevacuator.entities.Room
import com.nilmedov.smartevacuator.utils.*
import com.nilmedov.smartevacuator.views.GraphFloorView
import org.graphstream.algorithm.AStar
import org.graphstream.graph.Node
import org.graphstream.graph.implementations.AbstractEdge
import org.graphstream.graph.implementations.AbstractGraph
import org.graphstream.graph.implementations.AbstractNode
import org.graphstream.graph.implementations.DefaultGraph
import org.graphstream.ui.view.ViewerListener
import org.jetbrains.annotations.NotNull

class GraphFloorScreen : GraphFloorView, ViewerListener {

    companion object {
        private val TAG = GraphFloorScreen::class.java.simpleName

        val GRAPH_ID = "GRAPH_1"
    }

    private var mGraph : AbstractGraph? = null
    private var mGraphInfo: GraphInfo? = null
    private var mBestPath: List<Node>? = null


    override fun setFloorGraph(graphInfo: GraphInfo) {
        mGraphInfo = graphInfo
        val graph = createGraph()
        mGraph = graph
        val edges = HashSet<Pair<String, String>>()
        with(graph) {
            addAttribute("ui.quality")
            addAttribute("ui.antialias")
            ResourceUtils.getCSSPath()?.run {
                Log.d(TAG, "css path - $this")
                addAttribute("ui.stylesheet", this)
            }
        }
        for (exit in graphInfo.exits) {
            graph.addNode<AbstractNode>(exit.id)
                    .setStoredNode(exit)
                    .setName(exit.id)
                    .setStyle(ResourceUtils.CSS_STYLE_EXIT)
            exit.neighbors.mapTo(edges) { Pair(exit.id, it) }
        }
        for (room in graphInfo.rooms) {
            graph.addNode<AbstractNode>(room.id)
                    .setStoredNode(room)
                    .setName(room.id)
                    .setStyle(ResourceUtils.CSS_STYLE_ROOM)
            room.neighbors.mapTo(edges) { Pair(room.id, it) }
        }
        for (cell in graphInfo.cells) {
            graph.addNode<AbstractNode>(cell.id)
                    .setStoredNode(cell)
                    .setName(cell.id)
            cell.neighbors.mapTo(edges) { Pair(cell.id, it) }
        }
        for(edge in edges) {
            graph.addEdge<AbstractEdge>(edge.first + edge.second, edge.first, edge.second)
        }

        val viewer = graph.display()
        viewer.defaultView.resizeFrame(SystemUtils.DISPLAY_MODE.width, SystemUtils.DISPLAY_MODE.height)
        val pipe = viewer.newViewerPipe()
        pipe.addSink(graph)
        pipe.addViewerListener(this)
        EventHandler.submit(pipe)
    }

    override fun buttonPushed(s: String?) {
        Log.d(TAG, "buttonPushed() $s")
        if (mBestPath != null) {
            hidePath()
        } else {
            showPathToExit(s)
        }
    }

    override fun buttonReleased(s: String?) {
        Log.d(TAG, "buttonReleased() $s")
    }

    override fun viewClosed(s: String?) {
        EventHandler.invalidate()
    }

    @NotNull
    private fun createGraph() : AbstractGraph {
        val graph = DefaultGraph(GRAPH_ID)
        with(graph) {
            addAttribute("ui.quality")
            addAttribute("ui.antialias")
        }

        return graph
    }

    private fun showPathToExit(graphId: String?) {
        if (graphId == null) {
            return
        }

        val pathFinder = AStar(mGraph)
        mBestPath = null
        var bestPath: List<Node>? = null

        for (exit in mGraphInfo!!.exits) {
            pathFinder.compute(graphId, exit.id)
            val path = pathFinder.shortestPath.nodePath
            if (bestPath == null || bestPath.size > path.size) {
                bestPath = path
            }
        }

        mBestPath = bestPath
        Log.d(TAG, "Shortest path is $mBestPath")

        mBestPath?.forEach {
            it.setStyle(ResourceUtils.CSS_STYLE_PATH)
        }
    }

    private fun hidePath() {
        mBestPath?.forEach {
            when (it.getStoredNode()) {
                is Exit -> it.setStyle(ResourceUtils.CSS_STYLE_EXIT)
                is Room -> it.setStyle(ResourceUtils.CSS_STYLE_ROOM)
                is Cell -> it.setStyle(ResourceUtils.CSS_STYLE_NODE)
            }
        }

        mBestPath = null
    }

    override fun release() {
    }
}