package com.nilmedov.smartevacuator.entities


data class Room(
        override val id: String = "R",
        val peoples: Int = 0,
        override val neighbors: List<String> = ArrayList()) : Node()